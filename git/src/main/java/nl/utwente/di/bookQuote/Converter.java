package nl.utwente.di.bookQuote;

public class Converter {
    public int converter(String C) {
        int c = Integer.parseInt(C);
        return (int) (c *1.8 + 32);
    }
}
